﻿namespace Otus.MS.Web
{
    /// <summary>
    /// Базовый ответ от сервиса
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        /// Статус обработки запроса
        /// </summary>
        public string Status { get; set; }
    }
}