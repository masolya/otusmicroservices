﻿using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;

namespace Otus.MS.Web.Controllers
{
    public class HealthController : ApiController
    {
        // GET api/values
        /// <summary>
        /// http-метод GET /health/ RESPONSE: {"status": "OK"}
        /// </summary>
        /// <returns></returns>
        public BaseResponse Get()
        {
            var result = new BaseResponse
            {
                Status = "OK"
            };

            return result;
        }

        //// GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/values/5
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/values/5
        //public void Delete(int id)
        //{
        //}
    }
}
